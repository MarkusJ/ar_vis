﻿using UnityEngine;
using System.Collections.Generic;
using HoloToolkit.Sharing;
using System;
using System.Collections;

public class RemoteCursorManager : MonoBehaviour {
    public class RemoteCursorInstance
    {
        public long UserID;
        public GameObject AssociatedRemoteCursor;
    }

    Dictionary<long, RemoteCursorInstance> cursors = new Dictionary<long, RemoteCursorInstance>();
	void Start () {
        //Initialize Messagehandlers
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.CursorPosition] = this.OnCursorReceived;
        SharingSessionTracker.Instance.SessionLeft += Instance_SessionLeft;

    }

    void Update () {
	
	}

    private void OnCursorReceived(NetworkInMessage msg)
    {
        //Read User ID from message
        long id = msg.ReadInt64();

        //Generate a new remote cursor if needed, or update an existing one
        RemoteCursorInstance cursor;
        Transform anchor = ImportExportAnchorManager.Instance.gameObject.transform;
        if (!this.cursors.TryGetValue(id, out cursor))
        {
            //Create new cursor, set model, size and initial position
            cursor = new RemoteCursorInstance();
            cursor.UserID = id;
            cursor.AssociatedRemoteCursor = GameObject.CreatePrimitive(PrimitiveType.Sphere); //TODO Add a better model
            cursor.AssociatedRemoteCursor.transform.localScale = new Vector3 (0.025F, 0.025F, 0.025F);
            cursor.AssociatedRemoteCursor.transform.position = anchor.TransformPoint(CustomMessages.Instance.ReadVector3(msg));
            cursor.AssociatedRemoteCursor.transform.rotation = CustomMessages.Instance.ReadQuaternion(msg);
            cursors.Add(id, cursor);
            foreach (Collider c in cursor.AssociatedRemoteCursor.GetComponents<Collider>())
            {
                c.enabled = false;
            }
            foreach (Renderer r in cursor.AssociatedRemoteCursor.GetComponents<Renderer>())
            {
                r.enabled = false;
            }
        }
        else
        {
            //Update position of cursor
            if(AppStateManager.Instance.CurrentAppState == AppStateManager.AppState.Ready)
            {
                foreach (Renderer r in cursor.AssociatedRemoteCursor.GetComponents<Renderer>())
                {
                    r.enabled = true;
                }
            }
            cursor.AssociatedRemoteCursor.transform.position = Vector3.Lerp(cursor.AssociatedRemoteCursor.transform.position, anchor.TransformPoint(CustomMessages.Instance.ReadVector3(msg)), 0.06f);
            cursor.AssociatedRemoteCursor.transform.rotation = CustomMessages.Instance.ReadQuaternion(msg);
        }
    }

    //Destroy cursor if associated user leaves
    private void Instance_SessionLeft(object sender, SharingSessionTracker.SessionLeftEventArgs e)
    {
        RemoteCursorInstance cursor;
        //Get cursor for user id
        if (this.cursors.TryGetValue(e.exitingUserId, out cursor))
        {
            Destroy(cursor.AssociatedRemoteCursor);
            this.cursors.Remove(e.exitingUserId);
        }
    }
}
