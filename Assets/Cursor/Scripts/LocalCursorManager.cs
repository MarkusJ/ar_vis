﻿using UnityEngine;
using System.Collections;
using HoloToolkit.Sharing;
using System;


public class LocalCursorManager : MonoBehaviour {
    [Tooltip("The distance the cursor needs to travel before its' position will be logged")]
    public float DistBetweenPosLogs = 0.1f;
    //Last position of cursor
    public Vector3 lastPos { get; private set; }
	// Initialize
	void Start () {
        lastPos = transform.localPosition;
    }

    // Update is called once per frame
    void Update () {
        //Send cursor position to other users
        Transform anchor = ImportExportAnchorManager.Instance.gameObject.transform;
        CustomMessages.Instance.SendCursorPosition(anchor.InverseTransformPoint(transform.position), transform.rotation);

        //After the cursor traveled a certain distance, log its' position
        if(Vector3.Distance(lastPos, transform.localPosition) >= 0.1f)
        {
            GlobalExperimentLogger.Instance.WriteCursorPosition(transform.position);
            lastPos = transform.position;
        }
    }
    
}
