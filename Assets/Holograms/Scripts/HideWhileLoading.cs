﻿using UnityEngine;
using System.Collections;

public class HideWhileLoading : MonoBehaviour {
    private Renderer[] renderers;
    private Collider[] colliders;
    private bool doneEnabling = false;
    // Hide object and all children
    void Start () {
        renderers = gameObject.GetComponentsInChildren<Renderer>();
        colliders = gameObject.GetComponentsInChildren<Collider>();

        foreach (Renderer r in renderers)
        {
            r.enabled = false;
        }
        foreach(Collider c in colliders)
        {
            c.enabled = false;
        }
    }
	
	// Reactivate objects if AppState is ready
	void Update () {
        if (AppStateManager.Instance.CurrentAppState == AppStateManager.AppState.Ready && !doneEnabling)
        {
            foreach( Renderer r in renderers)
            {
                r.enabled = true;
            }
            foreach (Collider c in colliders)
            {
                c.enabled = true;
            }
            doneEnabling = true;
        }

    }
}
