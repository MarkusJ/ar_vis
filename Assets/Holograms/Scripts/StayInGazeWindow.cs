﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayInGazeWindow : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Keep instructions in field of view of the user
	void Update () {
        Vector3 moveTo = Camera.main.transform.position + (Camera.main.transform.forward) - (Camera.main.transform.right*0.2f) + (Camera.main.transform.up*0.08f);
        transform.position = Vector3.Lerp(transform.position, moveTo, 0.2f);
        transform.rotation = Camera.main.transform.localRotation;
    }
}
