﻿using HoloToolkit.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialAreaPlacement : MonoBehaviour
{

    private bool initialPlacementDone = false;
    private HologramSharedData sharedData;
    private int sharedHologramID;
    private bool isShared;
    private bool canBeOccupied;
    private bool receivedInitialPosition = false;
    private Vector3 receivedVector = new Vector3();
    private Quaternion receivedRotation = new Quaternion();
    private bool canBroadcast = false;


    // Use this for initialization
    void Start()
    {
        SharingSessionTracker.Instance.SessionJoined += Instance_SessionJoined;
    }

    // Update is called once per frame
    void Update()
    {
        if (!receivedInitialPosition)
        {
            //Get all walls, find the closest one
            if (!initialPlacementDone && AppStateManager.Instance.CurrentAppState == AppStateManager.AppState.Ready)
            {
                List<GameObject> walls = SurfaceMeshesToPlanes.Instance.GetActivePlanes(PlaneTypes.Wall);
                GameObject closestWall = new GameObject();
                float tmpDist = float.PositiveInfinity;
                foreach (GameObject wall in walls)
                {
                    float currentWallDist = Vector3.Distance(wall.transform.position, Camera.main.transform.position);
                    if (currentWallDist < tmpDist)
                    {
                        closestWall = wall;
                        tmpDist = currentWallDist;
                    }
                }

                //Set position to center of wall
                transform.position = closestWall.transform.position + (closestWall.GetComponent<SurfacePlane>().PlaneThickness * 2 * closestWall.GetComponent<SurfacePlane>().SurfaceNormal);
                transform.up = closestWall.GetComponent<SurfacePlane>().SurfaceNormal;
                initialPlacementDone = true;
                Transform anchor = ImportExportAnchorManager.Instance.gameObject.transform;
                CustomMessages.Instance.SendStageTransform(sharedHologramID, anchor.InverseTransformPoint(transform.position), transform.localRotation);
                canBroadcast = true;
            }
        }
        else
        {
            Transform anchor = ImportExportAnchorManager.Instance.gameObject.transform;
            transform.position = anchor.TransformPoint(receivedVector);
            transform.localRotation = receivedRotation;
            canBroadcast = true;
        }
    }

    /// <summary>
    /// Set initial parameters received from selector script
    /// </summary>
    /// <param name="l">List of parameters</param>
    void SetInitialParams(List<object> l)
    {
        sharedData = (HologramSharedData)l[0];
        sharedHologramID = (int)l[1];
        isShared = (bool)l[2];
        canBeOccupied = (bool)l[3];
    }

    /// <summary>
    /// When a remote system has a transform for us, we'll get it here.
    /// </summary>
    /// <param name="msgParams"></param>
    void OnStageTransform(List<object> msgParams)
    {
        
        //Read Hologram ID, compare
        int id = (int)msgParams[0];
        if (id == sharedHologramID)
        {
            receivedVector = (Vector3)msgParams[1];
            receivedRotation = (Quaternion)msgParams[2];
            receivedInitialPosition = true;
        }
    }
    /// <summary>
    /// Broadcast transform position/rotation for new clients
    /// </summary>
    /// <param name="sender">Joining user</param>
    /// <param name="e">Session args of joining user</param>
    private void Instance_SessionJoined(object sender, SharingSessionTracker.SessionJoinedEventArgs e)
    {
        if (isShared)
        {
            Transform anchor = ImportExportAnchorManager.Instance.gameObject.transform;
            if (canBroadcast)
            {
                CustomMessages.Instance.SendStageTransform(sharedHologramID, anchor.InverseTransformPoint(transform.position), transform.localRotation);
            }
        }
    }
}
