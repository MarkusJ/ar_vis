﻿using UnityEngine;
using System.Collections;
using UnityEngine.Windows.Speech;
using System.Collections.Generic;
using System.Linq;

public class TrailToggle : MonoBehaviour {

    KeywordRecognizer keywordRecognizer = null;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
    // Use this for initialization
    void Start()
    {
        keywords.Add("Toggle Trail", () => this.BroadcastMessage("ToggleTrail"));
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
        GetComponent<TrailRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update () {
	    
	}

    public void ToggleTrail()
    {
        TrailRenderer r = this.GetComponent < TrailRenderer >();
        if (r.enabled)
        {
            r.enabled = false;
        }
        else
        {
            r.enabled = true;
        }
    }
    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
}
