﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForExperiment : MonoBehaviour {

    public ExperimentStatusManager.ExperimentStatus WaitForStatus;

	// Use this for initialization
	void Start () {
		foreach(Renderer r in this.gameObject.GetComponentsInChildren<Renderer>())
        {
            r.enabled = false;
        }
        
	}
	
	// Update is called once per frame
	void Update () {
		if(ExperimentStatusManager.Instance.CurrentExperimentStatus == WaitForStatus)
        {
            foreach (Renderer r in this.gameObject.GetComponentsInChildren<Renderer>())
            {
                r.enabled = true;
            }
        }
        else
        {
            foreach (Renderer r in this.gameObject.GetComponentsInChildren<Renderer>())
            {
                r.enabled = false;
            }
        }
	}
}
