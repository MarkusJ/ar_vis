﻿using HoloToolkit.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperimentStatusManager : Singleton<ExperimentStatusManager> {

    //This enumeration contains every step of the experiment; add as needed
    public enum ExperimentStatus
    {
        Initializing = 0,
        WaitingForApp = 1,
        Tutorial = 2,
        WaitingForAllUsers = 3,
        Experiment = 4,
        
    }

    //Current step of experiment
    public ExperimentStatus CurrentExperimentStatus { get; set; }

    private int UserTutorialReadyCount = 0;
    private GameObject TextObject;
    private int userCount;

    void Start () {
        CurrentExperimentStatus = ExperimentStatus.Initializing;
        TextObject = GameObject.FindGameObjectWithTag("ExperimentText");
        SharingSessionTracker.Instance.SessionJoined += Instance_SessionJoined;
        SharingSessionTracker.Instance.SessionLeft += Instance_SessionLeft;
    }


    /// <summary>
    /// Count up joining users
    /// </summary>
    /// <param name="sender">Joining user</param>
    /// <param name="e">Session args of joining user</param>
    private void Instance_SessionJoined(object sender, SharingSessionTracker.SessionJoinedEventArgs e)
    {
        userCount += 1;
        Debug.Log("User count: " + userCount);
    }

    /// <summary>
    /// Count down leaving users
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Instance_SessionLeft(object sender, SharingSessionTracker.SessionLeftEventArgs e)
    {
        userCount -= 1;
    }

    void Update () {
        switch (CurrentExperimentStatus)
        {
            //We're still waiting for a connection
            case ExperimentStatus.Initializing:
                Debug.Log("ExperimentStatusHandler: App started. Waiting for App initialization to complete.");
                CurrentExperimentStatus = ExperimentStatus.WaitingForApp;
                break;
            //The app has loaded and we can display a tutorial
            case ExperimentStatus.WaitingForApp:
                if(AppStateManager.Instance.CurrentAppState == AppStateManager.AppState.Ready)
                {
                    Debug.Log("ExperimentStatusHandler: App ready, starting tutorial");
                    TextObject.GetComponent<TextMesh>().text = "Please find and select the cube" + Environment.NewLine + "and place it in the target area";
                    CurrentExperimentStatus = ExperimentStatus.Tutorial;
                }
                break;
                //We're waiting for all users to finish their respective tutorials
            case ExperimentStatus.WaitingForAllUsers:
                TextObject.GetComponent<TextMesh>().text = "Your tutorial is done, please wait" + Environment.NewLine + "for all other users to finish";
                if (UserTutorialReadyCount == userCount)
                {
                    CurrentExperimentStatus = ExperimentStatus.Experiment;
                    Debug.Log("ExperimentStatusHandler: All users are ready. Starting experiment");
                    TextObject.GetComponent<TextMesh>().text = "Please do the experiment:";
                }
                break;
            //The tutorial was finished and we can start the actual experiment
            case ExperimentStatus.Experiment:
                
                break;
        }
	}

    //Count up users who are done with their tutorials
    public void OnTutorialFinished()
    {
        UserTutorialReadyCount += 1;
        Debug.Log("Somebody finished. Count: " + UserTutorialReadyCount);
    }
}
