﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmPlacement : MonoBehaviour {

    //Step counter for internal status
    private int step = 0;

    //time counter
    float time = 0;

    [Tooltip("Select the layers the raycast should target.")]
    public LayerMask RaycastLayerMask = Physics.DefaultRaycastLayers;



    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        {
            RaycastHit hitInfo;
            bool hit;

            //Calculate surface hit
            hit = Physics.Raycast(transform.position,
                                transform.forward,
                                out hitInfo,
                                1,
                                RaycastLayerMask);

            //Triggers when the object is in the target area
            if (hit)
            {
                //Stepcounter will increase if certain requirements are met
                if (hitInfo.collider == GameObject.FindGameObjectWithTag("PlacementArea").GetComponent<Collider>() && (hitInfo.distance < 0.15f))
                {
                    switch (ExperimentStatusManager.Instance.CurrentExperimentStatus)
                    {
                        //Case: Tutorial active
                        case ExperimentStatusManager.ExperimentStatus.Tutorial:
                            //Initial hit
                            //Count up to one second, change status message, replace cube
                            if (step == 0)
                            {
                                time += Time.deltaTime;
                                if (time > 1.0f)
                                {
                                    step++;
                                    time = 0f;
                                    SendMessage("SetCube");
                                    GameObject.FindGameObjectWithTag("ExperimentText").GetComponent<TextMesh>().text = "Good!" + Environment.NewLine + "Now, please do it again";
                                }
                            }
                            //Second successful placement
                            //Wait for one second again, jump to first experiment step
                            else if (step == 1)
                            {
                                time += Time.deltaTime;
                                if (time > 1.0f)
                                {
                                    step++;
                                    time = 0f;
                                    CustomMessages.Instance.SendTutorialFinished();
                                    GameObject.FindGameObjectWithTag("ExperimentStatus").SendMessage("OnTutorialFinished");
                                    ExperimentStatusManager.Instance.CurrentExperimentStatus = ExperimentStatusManager.ExperimentStatus.WaitingForAllUsers;
                                    
                                }
                            }
                            break;
                        //Case: Experiment active
                        case ExperimentStatusManager.ExperimentStatus.Experiment:
                            //TODO
                            break;
                    }
                    

                }

            }

        }
    }
}
