﻿using UnityEngine;
using System.Collections.Generic;
using HoloToolkit.Unity;
using HoloToolkit.Sharing;
using System;


public class HologramSelect : Singleton<HologramSelect> {

    //Unity-configurable variables
    [Tooltip("Whether the object should be shared with other clients or not")]
    public bool IsShared;

    [Tooltip("Whether this object should be occupiable or not")]
    public bool CanBeOccupied;

    [Tooltip("The ID of the object; needed for sharing.")]
    public int SharedHologramID;

    [Tooltip("Whether this object can be selected by gazing at it")]
    public bool GazeSelection = false;

    [Tooltip("How long to gaze at this object to select it (in seconds).")]
    public float GazeSelectionDuration = 0;

    [Tooltip("What selecting this object will do")]
    public SelectorMode CurrentMode = SelectorMode.Select;

    //Data received from selector script
    HologramSharedData sharedData = new HologramSharedData();

    //Other global variables
    private long localUserID;
    public bool canBroadcast { get; private set; }
    public bool isSelected { get; private set; }
    public bool iSelected { get; private set; }
    public bool exclusive { get; private set; }
    public int userCount { get; private set; }
    public int selectionCount { get; private set; }
    public bool gazeSelectionTimerEnabled { get; private set; }
    public bool wrongSelectionTimerEnabled { get; private set; }
    public float gazeSelectionDurationTmp { get; private set; }
    public float wrongSelectionDurationTmp { get; private set; }


    //Enum for all actions a selection gesture may induce
    public enum SelectorMode
    {
        Select = 0,
        MultiSelect = 1,
        Exclusive = 2,
        Place = 3,
        WrongSelection = 4,
        UnSelectable = 5
    }

    public enum test
    {
        test,
        test2
    }

    void Start() {

        //userCount should be 0 since we also receive connection information about ourselves
        userCount = 0;

        //Initialize timers and status variables
        wrongSelectionTimerEnabled = false;
        gazeSelectionTimerEnabled = false;
        gazeSelectionDurationTmp = GazeSelectionDuration;
        iSelected = false;
        isSelected = false;

        //Initialize Session-Listeners
        if (IsShared)
        {

            SharingSessionTracker.Instance.SessionJoined += Instance_SessionJoined;
            SharingSessionTracker.Instance.SessionLeft += Instance_SessionLeft;

            if (SharedHologramID == 0)
            {
                throw (new Exception("WARNING: Object ID not set."));
            }
        }
        
        //Get our session user ID
        localUserID = SharingStage.Instance.Manager.GetLocalUser().GetID();
    }


    void Awake()
    {
        //Distribute initial parameters to other scripts
        List<object> initialParams = new List<object>();
        initialParams.Add(sharedData);
        initialParams.Add(SharedHologramID);
        initialParams.Add(IsShared);
        initialParams.Add(CanBeOccupied);
        this.SendMessage("SetInitialParams", initialParams);
    }

    
    void Update () {
        //Get renderer for later
        Renderer r = GetComponent<Renderer>();

        //Timer functionality
        if (wrongSelectionTimerEnabled)
        {
            if (wrongSelectionDurationTmp > 0)
            {
                wrongSelectionDurationTmp -= Time.deltaTime;
            }
            else
            {
                wrongSelectionTimerEnabled = false;
            }
        }
        else if (gazeSelectionTimerEnabled)
        {
            gazeSelectionDurationTmp -= Time.deltaTime;
            if (gazeSelectionDurationTmp <= 0)
            {
                OnSelect();
                gazeSelectionTimerEnabled = false;
                gazeSelectionDurationTmp = GazeSelectionDuration;
            }
        }

        //Multiselection trigger: Triggers when all users have selected the "correct" hologram
        if (CurrentMode == SelectorMode.MultiSelect)
        {
            if (selectionCount == userCount && !isSelected)
            {
                isSelected = true;
                GlobalExperimentLogger.Instance.WriteEvent(localUserID, SharedHologramID, GlobalExperimentLogger.LoggingEvents.ExperimentFinished);
            }
            else if (selectionCount != userCount)
            {
                isSelected = false;
            }
        }
        //Single selection trigger: Triggers when selected
        else
        {
            if (selectionCount == 1)
            {
                isSelected = true;
            }
            else
            {
                isSelected = false;
            }
        }
        
        //Coloration based on hologram status
        if (sharedData.OccupiedBy != 0 && sharedData.OccupiedBy != localUserID)
        {
            r.material.SetColor("_SpecColor", Color.red);
        }
        else
        {
            if(iSelected && !isSelected)
            {
                Color tmpColor = new Color(0.5F, 1, 0.5F);
                r.material.SetColor("_SpecColor", tmpColor);
            }
            else if (isSelected)
            {
                r.material.SetColor("_SpecColor", Color.green);
            }
            else if (wrongSelectionTimerEnabled)
            {
                r.material.SetColor("_SpecColor", Color.red);
            }
            else
            {
                r.material.SetColor("_SpecColor", Color.white);
            }
        }
        
    }

    /// <summary>
    /// Handles selection and calls fitting methods
    /// </summary>
    void OnSelect()
    {
        //TODO handle broadcasting when exclusivity is enabled/disabled
        //if (!exclusive) { }

        //Wait for app state to be ready
        if (AppStateManager.Instance.CurrentAppState == AppStateManager.AppState.Ready)
        {
            if (!canBroadcast)
            {
               canBroadcast = true;
            }

            //Act based on configured selection mode
            switch (CurrentMode)
            {
                //Standard case; will (de-)select a hologram
                case SelectorMode.Select:
                    Select();
                    break;
                //Will count up as users select this hologram
                case SelectorMode.MultiSelect:
                    MultiSelect();
                    break;
                //Here, we want to start selecting objects which we want to edit without sharing our changes immediately
                case SelectorMode.Exclusive:
                    ToggleExclusivity();
                    break;
                //If we want to move an object
                case SelectorMode.Place:
                    this.SendMessage("InitPlacement");
                    break;
                //For experiments: Triggered if a user fails to select the "right" hologram    
                case SelectorMode.WrongSelection:
                    WrongSelection();
                    break;
                case SelectorMode.UnSelectable:
                    break;
            }
        }
    }

    /// <summary>
    /// Start timing upon gaze entering
    /// </summary>
    private void OnGazeEnter()
    {
        if (GazeSelection && !wrongSelectionTimerEnabled)
        {
            gazeSelectionTimerEnabled = true;
        }
    }

    /// <summary>
    /// Stop and reset timing when gaze leaves
    /// </summary>
    private void OnGazeLeave()
    {
        if (GazeSelection)
        {
            gazeSelectionTimerEnabled = false;
            gazeSelectionDurationTmp = GazeSelectionDuration;
        }
    }

    // Methods called from selection

    /// <summary>
    /// Marks hologram as selected; transmits this to others
    /// </summary>
    void Select()
    {
        if (selectionCount >= 1)
        {
            selectionCount = 0;
        }
        else
        {
            selectionCount = 1;
        }
        CustomMessages.Instance.SendSelected(SharedHologramID, selectionCount);
        GlobalExperimentLogger.Instance.WriteEvent(localUserID, SharedHologramID, GlobalExperimentLogger.LoggingEvents.SelectSingle);
    }

    /// <summary>
    /// Marks object as selected by me, increases selection counters and trasmits it to others
    /// </summary>
    void MultiSelect()
    {
        if (!iSelected)
        {
            iSelected = !iSelected;
            selectionCount += 1;
            CustomMessages.Instance.SendSelected(SharedHologramID, selectionCount);
            GlobalExperimentLogger.Instance.WriteEvent(localUserID, SharedHologramID, GlobalExperimentLogger.LoggingEvents.SelectMulti);
        }
    }

    /// <summary>
    /// Will toggle exlusivity and set the object as occupied/unoccupied if needed
    /// </summary>
    public void ToggleExclusivity()
    {
        if (!exclusive)
        {
            sharedData.OccupiedBy = localUserID;
            CustomMessages.Instance.SendOccupied(1, SharedHologramID);
        }
        else
        {
            sharedData.OccupiedBy = 0;
            CustomMessages.Instance.SendOccupied(0, SharedHologramID);
        }
        exclusive = !exclusive;
        GlobalExperimentLogger.Instance.WriteEvent(localUserID, SharedHologramID, GlobalExperimentLogger.LoggingEvents.SelectExclusive);
    }

    //Resets and enables timer
    void WrongSelection()
    {
        wrongSelectionDurationTmp = 1;
        wrongSelectionTimerEnabled = true;
        GlobalExperimentLogger.Instance.WriteEvent(localUserID, SharedHologramID, GlobalExperimentLogger.LoggingEvents.WrongSelection);
    }



    /// <summary>
    /// Broadcast transform position/rotation for new clients
    /// </summary>
    /// <param name="sender">Joining user</param>
    /// <param name="e">Session args of joining user</param>
    private void Instance_SessionJoined(object sender, SharingSessionTracker.SessionJoinedEventArgs e)
    {
        userCount += 1;
        //CustomMessages.Instance.SendUserCount(e.joiningUser.GetID(), userCount);
        if (IsShared)
        {
            Transform anchor = ImportExportAnchorManager.Instance.gameObject.transform;
            if (canBroadcast)
            {
                CustomMessages.Instance.SendStageTransform(SharedHologramID, anchor.InverseTransformPoint(transform.position), transform.localRotation);
                if (sharedData.OccupiedBy == localUserID)
                {
                    CustomMessages.Instance.SendOccupied(1, SharedHologramID);
                }
            }
        }
        CustomMessages.Instance.SendSelected(SharedHologramID, selectionCount);

    }

    /// <summary>
    /// User leaves session; decrease user count, reset experiment status as it is no longer representative
    /// </summary>
    /// <param name="sender">Leaving user</param>
    /// <param name="e">Session args of leaving user</param>
    private void Instance_SessionLeft(object sender, SharingSessionTracker.SessionLeftEventArgs e)
    {
        userCount -= 1;
        iSelected = false;
        selectionCount = 0;
    }

    /// <summary>
    /// Called from remote selection handler; sets selectionCount to received value
    /// </summary>
    /// <param name="msgParams">List of received parameters</param>
    private void OnRemoteSelect(List<object> msgParams)
    {
        long uID = (long)msgParams[0];
        int hID = (int)msgParams[1];
        int count = (int)msgParams[2];

        //Check if we sent this, ourselves
        if (hID == SharedHologramID && uID != localUserID)
        {
            selectionCount = count;

            //Logging
            switch (CurrentMode)
            {
                case SelectorMode.Select:
                    GlobalExperimentLogger.Instance.WriteEvent(uID, hID, GlobalExperimentLogger.LoggingEvents.SelectSingle);
                    break;
                case SelectorMode.MultiSelect:
                    GlobalExperimentLogger.Instance.WriteEvent(uID, hID, GlobalExperimentLogger.LoggingEvents.SelectMulti);
                    break;
            }

        }
    }

    /// <summary>
    /// Handles other users wantint to occupy this hologram
    /// </summary>
    /// <param name="msgParams">List of received parameters</param>
    void OnOccupied(List<object> msgParams)
    {
        long occ = (long)msgParams[0];
        int i = (int)msgParams[1];
        int id = (int)msgParams[2];
        //Check if this adresses the hologram this script is attached to
        if (id == SharedHologramID)
        {
            //Toggle occupied status
            if (i == 0)
            {
                sharedData.OccupiedBy = 0;
            }
            else
            {
                sharedData.OccupiedBy = occ;
            }
        }
       
    }

    /// <summary>
    /// If we just joined, we will want to receive the current user count
    /// </summary>
    /// <param name="msgParams">List of received parameters</param>
    void OnUserCount(List<object> msgParams)
    {
        int id = (int)msgParams[0];
        int count = (int)msgParams[1];
        if (localUserID != id)
        {
            userCount = count;
        }
    }


}
