﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialTutorialCubePlacement : MonoBehaviour {

    private bool initialPlacementDone = false;
    private Vector3 resetPos = new Vector3();
    Vector3 resetNormal = new Vector3();
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!initialPlacementDone && AppStateManager.Instance.CurrentAppState == AppStateManager.AppState.Ready)
            {
                SetCube();
            }
        }

    /// <summary>
    /// Places the cube a very short distance from our target area
    /// </summary>
    void SetCube()
    {
        //Initial placement
        if (!initialPlacementDone)
        {
            List<GameObject> walls = SurfaceMeshesToPlanes.Instance.GetActivePlanes(PlaneTypes.Wall);
            GameObject closestWall = new GameObject();
            float tmpDist = float.PositiveInfinity;
            foreach (GameObject wall in walls)
            {
                float currentFloorDist = Vector3.Distance(wall.transform.position, Camera.main.transform.position);
                if (currentFloorDist < tmpDist)
                {
                    closestWall = wall;
                    tmpDist = currentFloorDist;
                }
            }
            resetPos = closestWall.transform.position;
            resetNormal = closestWall.GetComponent<SurfacePlane>().SurfaceNormal;

            transform.position = resetPos + resetNormal * 1 + UnityEngine.Random.insideUnitSphere * 0.5f;
            transform.rotation = UnityEngine.Random.rotation;
            initialPlacementDone = true;
        }
        //Placement after initial placement; can be called any number of times needed
        else
        {
            transform.position = resetPos + resetNormal * 1 + UnityEngine.Random.insideUnitSphere * 0.5f;
            transform.rotation = UnityEngine.Random.rotation;
        }
        
    }
}
