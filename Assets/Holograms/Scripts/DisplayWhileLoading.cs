﻿using UnityEngine;
using System.Collections;

public class DisplayWhileLoading : MonoBehaviour
{
    private Renderer[] renderers;
    // Hide object and all children
    void Start()
    {
        renderers = gameObject.GetComponentsInChildren<Renderer>();

        foreach (Renderer r in renderers)
        {
            r.enabled = true;
        }
    }

    // Reactivate objects if AppState is ready
    void Update()
    {
        Vector3 moveTo;
        if (AppStateManager.Instance.CurrentAppState == AppStateManager.AppState.Ready)
        {
            foreach (Renderer r in renderers)
            {
                r.enabled = false;
            }
        } else
        {
            moveTo = Camera.main.transform.position + (Camera.main.transform.forward);
            transform.position = Vector3.Lerp(transform.position, moveTo, 0.2f);
            transform.rotation = Camera.main.transform.localRotation;
        }

    }
}
