﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Windows.Speech;
using HoloToolkit.Unity;
using HoloToolkit.Sharing;
using HoloToolkit;
using System;

public enum PlacementSurfaces
{
    // Horizontal surface with an upward pointing normal.    
    Horizontal = 1,

    // Vertical surface with a normal facing the user.
    Vertical = 2,

    //Freely, barring walls and floors
    Freely = 3,
}

public class HologramPlacement : Singleton<HologramPlacement>
{
    [Tooltip("The base material used to render the bounds asset when placement is allowed.")]
    public Material PlaceableBoundsMaterial = null;

    [Tooltip("The base material used to render the bounds asset when placement is not allowed.")]
    public Material NotPlaceableBoundsMaterial = null;

    [Tooltip("The material used to render the placement shadow when placement it allowed.")]
    public Material PlaceableShadowMaterial = null;

    [Tooltip("The material used to render the placement shadow when placement it not allowed.")]
    public Material NotPlaceableShadowMaterial = null;

    [Tooltip("The type of surface on which the object can be placed.")]
    public PlacementSurfaces PlacementSurface = PlacementSurfaces.Horizontal;

    [Tooltip("The child object(s) to hide during placement.")]
    public List<GameObject> ChildrenToHide = new List<GameObject>();

    [Tooltip("Whether the object is directly selectable (or will need an outside SendMessage)")]
    public bool Selectable = true;

    [Tooltip("If the object should follow the user around")]
    public bool Follow = false;

    [Tooltip("The distance in meters from the camera for the Tagalong to seek when updating its position.")]
    public float TagalongDistance = 2.0f;

    [Tooltip("If true, forces the Tagalong to be TagalongDistance from the camera, even if it didn't need to move otherwise.")]
    public bool EnforceDistance = true;

    [Tooltip("The speed at which to move the Tagalong when updating its position (meters/second).")]
    public float PositionUpdateSpeed = 9.8f;

    [Tooltip("When true, the Tagalong's motion is smoothed.")]
    public bool SmoothMotion = true;

    [Range(0.0f, 1.0f), Tooltip("The factor applied to the smoothing algorithm. 1.0f is super smooth. But slows things down a lot.")]
    public float SmoothingFactor = 0.75f;

    [Tooltip("Buffer size between Hologram and Surface.")]
    public float Buffer = 0.05f;

    public bool Exclusive { get; private set; }

    public HologramSharedData sharedData;


    // The BoxCollider represents the volume of the object that is tagging
    // along. It is a required component.
    protected BoxCollider tagalongCollider;

    // The Interpolator is a helper class that handles various changes to an
    // object's transform. It is used by Tagalong to adjust the object's
    // transform.position.
    protected Interpolator interpolator;

    // This is an array of planes that define the camera's view frustum along
    // with some helpful indices into the array. The array is updated each
    // time through FixedUpdate().
    protected Plane[] frustumPlanes;
    protected const int frustumLeft = 0;
    protected const int frustumRight = 1;
    protected const int frustumBottom = 2;
    protected const int frustumTop = 3;
    /// <summary>
    /// Tracks if we have been sent a transform for the anchor model.
    /// The anchor model is rendered relative to the actual anchor.
    /// </summary>
    public bool GotTransform { get; private set; }

    public bool IsPlacing { get; private set; }

    // The most recent distance to the surface.  This is used to 
    // locate the object when the user's gaze does not intersect
    // with the Spatial Mapping mesh.
    private float lastDistance = 2.0f;

    // The distance away from the target surface that the object should hover prior while being placed.
    private float hoverDistance = 0.15f;

    // Threshold (the closer to 0, the stricter the standard) used to determine if a surface is flat.
    private float distanceThreshold = 0.02f;

    // Threshold (the closer to 1, the stricter the standard) used to determine if a surface is vertical.
    private float upNormalThreshold = 0.9f;

    // Maximum distance, from the object, that placement is allowed.
    // This is used when raycasting to see if the object is near a placeable surface.
    private float maximumPlacementDistance = 20.0f;

    // Speed (1.0 being fastest) at which the object settles to the surface upon placement.
    private float placementVelocity = 0.26f;

    // Indicates whether or not this script manages the object's box collider.
    private bool managingBoxCollider = false;

    // The box collider used to determine of the object will fit in the desired location.
    // It is also used to size the bounding cube.
    private BoxCollider boxCollider = null;

    // Visible asset used to show the dimensions of the object. This asset is sized
    // using the box collider's bounds.
    private GameObject boundsAsset = null;

    // Visible asset used to show the where the object is attempting to be placed.
    // This asset is sized using the box collider's bounds.
    private GameObject shadowAsset = null;

    // The location at which the object will be placed.
    private Vector3 targetPosition;

    private long localUserID;

    public bool isShared { get; private set; }

    public bool canBeOccupied { get; private set; }

    public int sharedHologramID { get; private set; }

    void Start()
    {
       
        GotTransform = true;
        localUserID = SharingStage.Instance.Manager.GetLocalUser().GetID();

        // Make sure the Tagalong object has a BoxCollider.
        tagalongCollider = GetComponent<BoxCollider>();
        if (!tagalongCollider)
        {
            // If we can't find one, disable the script.
            enabled = false;
        }

        // Add an Interpolator component and set some default parameters for
        // it. These parameters can be adjusted in Unity's Inspector.
        interpolator = gameObject.AddComponent<Interpolator>();
        interpolator.SmoothLerpToTarget = SmoothMotion;
        interpolator.SmoothPositionLerpRatio = SmoothingFactor;
    }
    private void Awake()
    {
        targetPosition = transform.position;

        // Get the object's collider.
        boxCollider = gameObject.GetComponent<BoxCollider>();
        if (boxCollider == null)
        {
            // The object does not have a collider, create one and remember that
            // we are managing it.
            managingBoxCollider = true;
            boxCollider = gameObject.AddComponent<BoxCollider>();
            boxCollider.enabled = false;
        }

        // Create the object that will be used to indicate the bounds of the 
        boundsAsset = GameObject.CreatePrimitive(PrimitiveType.Cube);
        boundsAsset.transform.parent = transform;
        boundsAsset.SetActive(false);

        // Create a object that will be used as a shadow.
        shadowAsset = GameObject.CreatePrimitive(PrimitiveType.Quad);
        shadowAsset.transform.parent = transform;
        shadowAsset.SetActive(false);
        SendMessage("SetStatus", PlacementSurface);
        
    }

    void Update()
    {
        //Get anchor from ImportExportAnchorManager
        Transform anchor = ImportExportAnchorManager.Instance.gameObject.transform;

        //Objects will not be updated while the app state is not ready, yet
        if (AppStateManager.Instance.CurrentAppState == AppStateManager.AppState.Ready)
        {
            //Only update the position of this hologram if it's not already following the user
            if (!Follow && !interpolator.Running)
            {
                //User is placing the hologram
                if (IsPlacing)
                {
                    // Move the object.
                    Move();
                    Vector3 targetPosition;
                    Vector3 surfaceNormal;

                    //Validate placement target position
                    bool canBePlaced = ValidatePlacement(out targetPosition, out surfaceNormal);

                    //Special case for free placement mode
                    if (PlacementSurface != PlacementSurfaces.Freely)
                    {
                        DisplayBounds(canBePlaced);
                        DisplayShadow(targetPosition, surfaceNormal, canBePlaced);
                    }

                    //If the new position needs to be broadcasted
                    if (isShared)
                    {
                        CustomMessages.Instance.SendStageTransform(sharedHologramID, anchor.InverseTransformPoint(transform.position), transform.localRotation);
                    }
                }
                //User is NOT placing the hologram (any more)
                else
                {
                    // Disable the visual elements.
                    boundsAsset.SetActive(false);
                    shadowAsset.SetActive(false);

                    //Calculate distance from hovering object to target area
                    float dist = (transform.position - targetPosition).magnitude;

                    //User is the one occupying the object
                    if (sharedData.OccupiedBy == localUserID)
                    {
                        //Hologram is still being placed or traveling towards its' target location
                        if (dist > 0.005 && GotTransform == false)
                        {
                            //Update position
                            transform.position = Vector3.Lerp(gameObject.transform.position, targetPosition, placementVelocity / dist);

                            //If needed, send new position to other clients
                            if (isShared)
                            {
                                CustomMessages.Instance.SendStageTransform(sharedHologramID, anchor.InverseTransformPoint(transform.position), transform.localRotation);
                            }
                        }
                        //Hologram has arrived at target destination
                        else
                        {
                            //Set Transform, send Unoccupied to other clients (but only if exclusivity is deactivated)
                            GotTransform = true;

                            if (isShared)
                            {
                                CustomMessages.Instance.SendOccupied(0, sharedHologramID);
                            }

                            if (!Exclusive)
                            {
                                sharedData.OccupiedBy = 0;
                            }
                            
                            // Unhide the child object(s) to make placement easier.
                            for (int i = 0; i < ChildrenToHide.Count; i++)
                            {
                                ChildrenToHide[i].SetActive(true);
                            }
                        }
                    }
                    else
                    {
                        if (dist > 0.005 && GotTransform == false)
                        {
                            //Users not occupying an object should not be sending its position
                            transform.position = Vector3.Lerp(gameObject.transform.position, targetPosition, placementVelocity / dist);
                        }
                        else
                        {
                            GotTransform = true;
                        }
                    }
                }
            }
            //Tagalong
            else
            {
                // Retrieve the frustum planes from the camera.
                frustumPlanes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

                // Determine if the Tagalong needs to move based on whether its BoxCollider is in or out of the camera's view frustum.
                Vector3 tagalongTargetPosition;
                if (CalculateTagalongTargetPosition(transform.position, out tagalongTargetPosition))
                {
                    // Derived classes will use the same Interpolator and may have adjusted its PositionUpdateSpeed for some other purpose. Restore the value we care about and tell the Interpolator to move the Tagalong to its new target position.
                    interpolator.PositionPerSecond = PositionUpdateSpeed;
                    interpolator.SetTargetPosition(tagalongTargetPosition);
                    Quaternion tmpRotation;
                    tmpRotation = Camera.main.transform.rotation;
                    tmpRotation.x = 0f;
                    tmpRotation.z = 0f;
                    transform.rotation = tmpRotation;
                }
                else if (!interpolator.Running && EnforceDistance)
                {
                    // If the Tagalong is inside the camera's view frustum, and it is supposed to stay a fixed distance from the camera, force the tagalong to that location (without using the Interpolator).
                    Ray ray = new Ray(Camera.main.transform.position, transform.position - Camera.main.transform.position);
                    transform.position = ray.GetPoint(TagalongDistance);
                }
            }
        }
    }

    /// <summary>
    /// Save parameters received from selector script
    /// </summary>
    /// <param name="l"></param>
    public void SetInitialParams(List<object> l)
    {
        sharedData = (HologramSharedData)l[0];
        sharedHologramID = (int)l[1];
        isShared = (bool)l[2];
        canBeOccupied = (bool)l[3];
    }

    /// <summary>
    /// Called when there is an intention to move this hologram
    /// </summary>
    public void InitPlacement()
    {
        //Check for occupied status and if the object is selectable and not set as exclusive
        if (Selectable && !Exclusive)
        {
            if (!IsPlacing && sharedData.OccupiedBy == 0)
            {
                OnPlacementStart();
            }
            else if (IsPlacing)
            {
                OnPlacementStop();
            }

        }
        //Special case if the placement mode was triggered from somewhere else
        else if (IsPlacing)
        {
            OnPlacementStop();
        }
    }

    /// <summary>
    /// For selecting this hologram from somewhere else than this script
    /// </summary>
    public void RemoteSelect()
    {
        //Check for occupied status
        if (!IsPlacing && (sharedData.OccupiedBy == 0 || !canBeOccupied))
        {
            OnPlacementStart();
        }

    }

    /// <summary>
    /// Begin of placement action
    /// </summary>
    public void OnPlacementStart()
    {
        Follow = false;
        // If we are managing the collider, enable it. 
        if (managingBoxCollider)
        {
            boxCollider.enabled = true;
        }

        // Hide the child object(s) to make placement easier.
        for (int i = 0; i < ChildrenToHide.Count; i++)
        {
            ChildrenToHide[i].SetActive(false);
        }

        // Tell the gesture manager that it is to assume
        // all input is to be given to this object.
        GestureManager.Instance.OverrideFocusedObject = gameObject;

        if (isShared && canBeOccupied)
        {
            CustomMessages.Instance.SendOccupied(1, sharedHologramID);
        }
        sharedData.OccupiedBy = localUserID;

        // Enter placement mode.
        IsPlacing = true;
        GlobalExperimentLogger.Instance.WriteEvent(localUserID, sharedHologramID, GlobalExperimentLogger.LoggingEvents.SelectPlaceStart);
    }

    /// <summary>
    /// Take the object out of placement mode.
    /// </summary>
    /// <remarks>
    /// This method will leave the object in placement mode if called while
    /// the object is in an invalid location.  To determine whether or not
    /// the object has been placed, check the value of the IsPlacing property.
    /// </remarks>
    public void OnPlacementStop()
    {
        // ValidatePlacement requires a normal as an out parameter.
        Vector3 position;
        Vector3 surfaceNormal;

        // Check to see if we can exit placement mode.
        if (!ValidatePlacement(out position, out surfaceNormal))
        {
            return;
        }

        //Place Object
        float depth = 0.0f;
        if (PlacementSurface == PlacementSurfaces.Horizontal)
            depth = transform.localScale.y / 2;
        else if (PlacementSurface == PlacementSurfaces.Vertical)
            depth = transform.localScale.z / 2;

        targetPosition = position + ((depth + Buffer) * surfaceNormal);

        OrientObject(true, surfaceNormal);

        // If we are managing the collider, disable it. 
        if (managingBoxCollider)
        {
            boxCollider.enabled = false;
        }

        // Tell the gesture manager that it is to resume
        // its normal behavior.
        GestureManager.Instance.OverrideFocusedObject = null;

        // Exit placement mode.
        GotTransform = false;
        IsPlacing = false;
        GlobalExperimentLogger.Instance.WriteEvent(localUserID, sharedHologramID, GlobalExperimentLogger.LoggingEvents.SelectPlaceStop);
    }

    /// <summary>
    /// Moves the hologram
    /// </summary>
    private void Move()
    {
        Vector3 moveTo = transform.position;
        Vector3 surfaceNormal = Vector3.zero;
        RaycastHit hitInfo;
        bool hit;

        //Calculate surface hit
        hit = Physics.Raycast(Camera.main.transform.position,
                            Camera.main.transform.forward,
                            out hitInfo,
                            maximumPlacementDistance,
                            SpatialMappingManager.Instance.LayerMask);

        //We're not placing freely
        if (PlacementSurface != PlacementSurfaces.Freely)
        {
            if (hit)
            {
                //Calculate halved height/depth of transform to offset the hover distance
                float halfHeight = 0;
                if (PlacementSurface == PlacementSurfaces.Horizontal)
                {
                    halfHeight = transform.localScale.y / 2;
                }
                else if (PlacementSurface == PlacementSurfaces.Vertical)
                {
                    halfHeight = transform.localScale.z / 2;
                }

                float offsetDistance = hoverDistance + halfHeight;

                // Place the object a small distance away from the surface while keeping 
                // the object from going behind the user.
                if (hitInfo.distance <= hoverDistance)
                {
                    offsetDistance = 0f;
                }

                moveTo = hitInfo.point + (offsetDistance * hitInfo.normal);

                lastDistance = hitInfo.distance;
                surfaceNormal = hitInfo.normal;
            }
            else
            {
                // The raycast failed to hit a surface.  In this case, keep the object at the distance of the last
                // intersected surface.
                moveTo = Camera.main.transform.position + (Camera.main.transform.forward * lastDistance);
            }
        }
        //Special case for free placement
        else
        {
            moveTo = Camera.main.transform.position + (Camera.main.transform.forward * 1.5F);
        }


        // Follow the user's gaze.
        transform.position = Vector3.Lerp(transform.position, moveTo, placementVelocity);

        // Orient the object.
        // We are using the return value from Physics.Raycast to instruct
        // the OrientObject function to align to the vertical surface if appropriate.
        OrientObject(hit, surfaceNormal);
    }

    /// <summary>
    /// Holograms need to be aligned to us or the placement surface
    /// </summary>
    /// <param name="alignToVerticalSurface"></param>
    /// <param name="surfaceNormal"></param>
    private void OrientObject(bool alignToVerticalSurface, Vector3 surfaceNormal)
    {
        Quaternion rotation = Camera.main.transform.localRotation;

        // If the user's gaze does not intersect with the Spatial Mapping mesh,
        // orient the object towards the user.
        if (alignToVerticalSurface && (PlacementSurface == PlacementSurfaces.Vertical))
        {
            // We are placing on a vertical surface.
            // If the normal of the Spatial Mapping mesh indicates that the
            // surface is vertical, orient parallel to the surface.
            if (Mathf.Abs(surfaceNormal.y) <= (1 - upNormalThreshold))
            {
                rotation = Quaternion.LookRotation(-surfaceNormal, Vector3.up);
            }
        }
        else
        {
            rotation.x = 0f;
            rotation.z = 0f;
        }

        transform.rotation = rotation;
    }

    /// <summary>
    /// Will check if a hologram can actually be placed
    /// </summary>
    /// <param name="position"></param>
    /// <param name="surfaceNormal"></param>
    /// <returns></returns>
    private bool ValidatePlacement(out Vector3 position, out Vector3 surfaceNormal)
    {
        Vector3 raycastDirection = gameObject.transform.forward;

        if (PlacementSurface == PlacementSurfaces.Horizontal)
        {
            // Placing on horizontal surfaces.
            // Raycast from the bottom face of the box collider.
            raycastDirection = -(Vector3.up);
        }

        // Initialize out parameters.
        position = Vector3.zero;
        surfaceNormal = Vector3.zero;

        Vector3[] facePoints = GetColliderFacePoints();

        // The origin points we receive are in local space and we 
        // need to raycast in world space.
        for (int i = 0; i < facePoints.Length; i++)
        {
            facePoints[i] = gameObject.transform.TransformVector(facePoints[i]) + gameObject.transform.position;
        }
        RaycastHit centerHit;

        //Special case: Freely place the object
        if (PlacementSurface == PlacementSurfaces.Freely)
        {
            //Check if facepoints are situated a certain distance away from all surfaces
            //Vertical surfaces
            for (int i = 1; i < facePoints.Length; i++)
            {
                RaycastHit hitInfo;
                Physics.Raycast(facePoints[i],
                                    raycastDirection,
                                    out hitInfo,
                                    maximumPlacementDistance,
                                    SpatialMappingManager.Instance.LayerMask);

                if (Mathf.Abs(hitInfo.distance) < 0.5F)
                {
                    return false;
                }
            }
            //Horizontal surfaces
            raycastDirection = -(Vector3.up);
            for (int i = 1; i < facePoints.Length; i++)
            {
                RaycastHit hitInfo;
                Physics.Raycast(facePoints[i],
                                    raycastDirection,
                                    out hitInfo,
                                    maximumPlacementDistance,
                                    SpatialMappingManager.Instance.LayerMask);

                if (Mathf.Abs(hitInfo.distance) < 0.5F)
                {
                    return false;
                }
            }

            position = gameObject.transform.position;
            return true;
        }

        // Cast a ray from the center of the box collider face to the surface.
        if (!Physics.Raycast(facePoints[0],
                        raycastDirection,
                        out centerHit,
                        maximumPlacementDistance,
                        SpatialMappingManager.Instance.LayerMask))
        {
            // If the ray failed to hit the surface, we are done.
            return false;
        }

        // We have found a surface.  Set position and surfaceNormal.
        position = centerHit.point;
        surfaceNormal = centerHit.normal;

        // Cast a ray from the corners of the box collider face to the surface.
        for (int i = 1; i < facePoints.Length; i++)
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(facePoints[i],
                                raycastDirection,
                                out hitInfo,
                                maximumPlacementDistance,
                                SpatialMappingManager.Instance.LayerMask))
            {
                // To be a valid placement location, each of the corners must have a similar
                // enough distance to the surface as the center point
                if (!IsEquivalentDistance(centerHit.distance, hitInfo.distance))
                {
                    return false;
                }
            }
            else
            {
                // The raycast failed to intersect with the target layer.
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Get facepoints of each corner of our current collider
    /// </summary>
    /// <returns></returns>
    private Vector3[] GetColliderFacePoints()
    {
        // Get the collider extents.  
        // The size values are twice the extents.
        Vector3 extents = boxCollider.size / 2;

        // Calculate the min and max values for each coordinate.
        float minX = boxCollider.center.x - extents.x;
        float maxX = boxCollider.center.x + extents.x;
        float minY = boxCollider.center.y - extents.y;
        float maxY = boxCollider.center.y + extents.y;
        float minZ = boxCollider.center.z - extents.z;
        float maxZ = boxCollider.center.z + extents.z;

        Vector3 center;
        Vector3 corner0;
        Vector3 corner1;
        Vector3 corner2;
        Vector3 corner3;

        if (PlacementSurface == PlacementSurfaces.Horizontal)
        {
            // Placing on horizontal surfaces.
            center = new Vector3(boxCollider.center.x, minY, boxCollider.center.z);
            corner0 = new Vector3(minX, minY, minZ);
            corner1 = new Vector3(minX, minY, maxZ);
            corner2 = new Vector3(maxX, minY, minZ);
            corner3 = new Vector3(maxX, minY, maxZ);
        }
        else
        {
            // Placing on vertical surfaces.
            center = new Vector3(boxCollider.center.x, boxCollider.center.y, maxZ);
            corner0 = new Vector3(minX, minY, maxZ);
            corner1 = new Vector3(minX, maxY, maxZ);
            corner2 = new Vector3(maxX, minY, maxZ);
            corner3 = new Vector3(maxX, maxY, maxZ);
        }

        return new Vector3[] { center, corner0, corner1, corner2, corner3 };
    }

    /// <summary>
    /// Calculate if each collider face point has roughly the same distance to our placement surface
    /// </summary>
    /// <param name="d1"></param>
    /// <param name="d2"></param>
    /// <returns></returns>
    private bool IsEquivalentDistance(float d1, float d2)
    {
        float dist = Mathf.Abs(d1 - d2);
        return (dist <= distanceThreshold);
    }

    /// <summary>
    /// Display the bounds of our hologram so the user knows where it can be placed
    /// </summary>
    /// <param name="canBePlaced"></param>
    private void DisplayBounds(bool canBePlaced)
    {
        // Ensure the bounds asset is sized and positioned correctly.
        boundsAsset.transform.localPosition = boxCollider.center;
        boundsAsset.transform.localScale = boxCollider.size;
        boundsAsset.transform.rotation = transform.rotation;

        // Apply the appropriate material.
        if (canBePlaced)
        {
            boundsAsset.GetComponent<Renderer>().sharedMaterial = PlaceableBoundsMaterial;
        }
        else
        {
            boundsAsset.GetComponent<Renderer>().sharedMaterial = NotPlaceableBoundsMaterial;
        }

        // Show the bounds asset.
        boundsAsset.SetActive(true);
    }

    /// <summary>
    /// Display a "shadow" of our hologram on the placement surface
    /// </summary>
    /// <param name="position"></param>
    /// <param name="surfaceNormal"></param>
    /// <param name="canBePlaced"></param>
    private void DisplayShadow(Vector3 position,
                            Vector3 surfaceNormal,
                            bool canBePlaced)
    {
        // Rotate the shadow so that it is displayed on the correct surface and matches the object.
        float rotationX = 0.0f;

        if (PlacementSurface == PlacementSurfaces.Horizontal)
        {
            rotationX = 90.0f;
        }

        Quaternion rotation = Quaternion.Euler(rotationX, transform.rotation.eulerAngles.y, 0);

        shadowAsset.transform.localScale = boxCollider.size;
        shadowAsset.transform.rotation = rotation;

        // Apply the appropriate material.
        if (canBePlaced)
        {
            shadowAsset.GetComponent<Renderer>().sharedMaterial = PlaceableShadowMaterial;
        }
        else
        {
            shadowAsset.GetComponent<Renderer>().sharedMaterial = NotPlaceableShadowMaterial;
        }

        // Show the shadow asset as appropriate.        
        if (position != Vector3.zero)
        {
            // Position the shadow a small distance from the target surface, along the normal.
            shadowAsset.transform.position = position + (0.01f * surfaceNormal);
            shadowAsset.SetActive(true);
        }
        else
        {
            shadowAsset.SetActive(false);
        }
    }


    /// <summary>
    /// Method for rotating through placement surfaces
    /// </summary>
    public void PickMovementMode()
    {
        switch (PlacementSurface)
        {  
            case PlacementSurfaces.Horizontal:
                PlacementSurface = PlacementSurfaces.Vertical;
                MovementModeButtonStatusText.Instance.SendMessage("SetStatus", PlacementSurfaces.Vertical);
                break;
            case PlacementSurfaces.Vertical:
                PlacementSurface = PlacementSurfaces.Freely;
                MovementModeButtonStatusText.Instance.SendMessage("SetStatus", PlacementSurfaces.Freely);
                break;
            case PlacementSurfaces.Freely:
                PlacementSurface = PlacementSurfaces.Horizontal;
                MovementModeButtonStatusText.Instance.SendMessage("SetStatus", PlacementSurfaces.Horizontal);
                break;
       }
    }

    /// <summary>
    /// Calculates where the hologram should float to when it is to follow us
    /// </summary>
    /// <param name="fromPosition"></param>
    /// <param name="toPosition"></param>
    /// <returns></returns>
    protected virtual bool CalculateTagalongTargetPosition(Vector3 fromPosition, out Vector3 toPosition)
    {
        // Check to see if any part of the Tagalong's BoxCollider's bounds is
        // inside the camera's view frustum. Note, the bounds used are an Axis
        // Aligned Bounding Box (AABB).
        bool needsToMove = !GeometryUtility.TestPlanesAABB(frustumPlanes, tagalongCollider.bounds);

        // Calculate a default position where the Tagalong should go. In this
        // case TagalongDistance from the camera along the gaze vector.
        toPosition = Camera.main.transform.position + Camera.main.transform.forward * TagalongDistance;

        // If we already know we don't need to move, bail out early.
        if (!needsToMove)
        {
            return false;
        }

        // Create a Ray and set it's origin to be the default toPosition that
        // was calculated above.
        Ray ray = new Ray(toPosition, Vector3.zero);
        Plane plane = new Plane();
        float distanceOffset = 0f;

        // Determine if the Tagalong needs to move to the right or the left
        // to get back inside the camera's view frustum. The normals of the
        // planes that make up the camera's view frustum point inward.
        bool moveRight = frustumPlanes[frustumLeft].GetDistanceToPoint(fromPosition) < 0;
        bool moveLeft = frustumPlanes[frustumRight].GetDistanceToPoint(fromPosition) < 0;
        if (moveRight)
        {
            // If the Tagalong needs to move to the right, that means it is to
            // the left of the left frustum plane. Remember that plane and set
            // our Ray's direction to point towards that plane (remember the
            // Ray's origin is already inside the view frustum.
            plane = frustumPlanes[frustumLeft];
            ray.direction = -Camera.main.transform.right;
        }
        else if (moveLeft)
        {
            // Apply similar logic to above for the case where the Tagalong
            // needs to move to the left.
            plane = frustumPlanes[frustumRight];
            ray.direction = Camera.main.transform.right;
        }
        if (moveRight || moveLeft)
        {
            // If the Tagalong needed to move in the X direction, cast a Ray
            // from the default position to the plane we are working with.
            plane.Raycast(ray, out distanceOffset);

            // Get the point along that ray that is on the plane and update
            // the x component of the Tagalong's desired position.
            toPosition.x = ray.GetPoint(distanceOffset).x;
        }

        // Similar logic follows below for determining if and how the
        // Tagalong would need to move up or down.
        bool moveDown = frustumPlanes[frustumTop].GetDistanceToPoint(fromPosition) < 0;
        bool moveUp = frustumPlanes[frustumBottom].GetDistanceToPoint(fromPosition) < 0;
        if (moveDown)
        {
            plane = frustumPlanes[frustumTop];
            ray.direction = Camera.main.transform.up;
        }
        else if (moveUp)
        {
            plane = frustumPlanes[frustumBottom];
            ray.direction = -Camera.main.transform.up;
        }
        if (moveUp || moveDown)
        {
            plane.Raycast(ray, out distanceOffset);
            toPosition.y = ray.GetPoint(distanceOffset).y;
        }

        // Create a ray that starts at the camera and points in the direction
        // of the calculated toPosition.
        ray = new Ray(Camera.main.transform.position, toPosition - Camera.main.transform.position);

        // Find the point along that ray that is the right distance away and
        // update the calculated toPosition to be that point.
        toPosition = ray.GetPoint(TagalongDistance);

        // If we got here, needsToMove will be true.
        return needsToMove;
    }

    /// <summary>
    /// Activates follow mode and forces hologram to follow us, once
    /// </summary>
    public void ToggleFollowMode()
    {
        
        if(!IsPlacing)
        {
            if (!Follow)
            {
                interpolator.PositionPerSecond = PositionUpdateSpeed;
                interpolator.SetTargetPosition(Camera.main.transform.position + Camera.main.transform.forward * TagalongDistance);
                Quaternion tmpRotation;
                tmpRotation = Camera.main.transform.rotation;
                tmpRotation.x = 0f;
                tmpRotation.z = 0f;
                transform.rotation = tmpRotation;
            }
            Follow = !Follow;
        }
    }

    /// <summary>
    /// When a remote system has a transform for us, we'll get it here.
    /// </summary>
    /// <param name="msgParams"></param>
    void OnStageTransform(List<object> msgParams)
    {
        Transform anchor = ImportExportAnchorManager.Instance.gameObject.transform;

        //Read Hologram ID, compare
        int id = (int)msgParams[0];
        if (id == sharedHologramID)
        {
            targetPosition = anchor.TransformPoint((Vector3)msgParams[1]);
            transform.localRotation = (Quaternion)msgParams[2];
        }
        GotTransform = false;
    }

    /// <summary>
    /// A client is ready to receive a transform; broadcast one.
    /// </summary>
    void OnClientReady()
    {
        Transform anchor = ImportExportAnchorManager.Instance.gameObject.transform;
        CustomMessages.Instance.SendStageTransform(sharedHologramID, anchor.InverseTransformPoint(transform.position), transform.localRotation);
    }
}