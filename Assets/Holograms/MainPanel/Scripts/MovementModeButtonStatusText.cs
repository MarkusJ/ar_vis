﻿using HoloToolkit.Sharing;
using HoloToolkit.Unity;
using System.Collections.Generic;
using UnityEngine;

public class MovementModeButtonStatusText : Singleton<MovementModeButtonStatusText> {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void SetStatus(PlacementSurfaces i)
    {
        switch (i) {
            case PlacementSurfaces.Horizontal:
                transform.GetComponent<TextMesh>().text = "H";
                break;
            case PlacementSurfaces.Vertical:
                transform.GetComponent<TextMesh>().text = "V";
                break;
            case PlacementSurfaces.Freely:
                transform.GetComponent<TextMesh>().text = "F";
                break;
        }
    }
}
