﻿using UnityEngine;
using System.Collections.Generic;
using HoloToolkit;
using HoloToolkit.Unity;
using HoloToolkit.Sharing;
using System;

public class MessageHandlers : Singleton<MessageHandlers>
{
    public List<float> userList = new List<float>();

    // Use this for initialization
    void Start()
    {
        //Listens for other clients becoming ready to receive coordinates
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.Selected] = this.RemoteSelect;

        //Listens for updates on whether Object to move is occupied or not
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.Occupied] = this.Occupied;

        // We care about getting updates for the anchor transform.
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.StageTransform] = this.StageTransform;

        //Listens for other clients becoming ready to receive coordinates
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.Ready] = this.ClientReady;

        //Listens for user count broadcasts
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.UserCount] = this.UserCount;

        //Listens for users who are done with their tutorials
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.TutorialFinished] = this.TutorialFinished;

        //Listens for users (dis-)connecting
        SharingSessionTracker.Instance.SessionJoined += Instance_SessionJoined;
        SharingSessionTracker.Instance.SessionLeft += Instance_SessionLeft;

    }

    /// <summary>
    /// Keep a list of connected users
    /// </summary>
    /// <param name="sender">Joining user</param>
    /// <param name="e">Session args of joining user</param>
    private void Instance_SessionJoined(object sender, SharingSessionTracker.SessionJoinedEventArgs e)
    {
        userList.Add(e.joiningUser.GetID());
        foreach(int user in userList)
        {
            Debug.Log("User: " + user);
        }
    }

    /// <summary>
    /// Clean up list of joined users on leave
    /// </summary>
    /// <param name="sender">Leaving user</param>
    /// <param name="e">Session args of leaving user</param>
    private void Instance_SessionLeft(object sender, SharingSessionTracker.SessionLeftEventArgs e)
    {
        userList.Remove(e.exitingUserId);
    }

    //Handles selections from other users
    private void RemoteSelect(NetworkInMessage msg)
    {
        //Read data, write into list, transmit via Message to each child
        long uID = msg.ReadInt64();
        int oID = msg.ReadInt32();
        int count = msg.ReadInt32();
        List<object> msgParams = new List<object>();
        msgParams.Add(uID);
        msgParams.Add(oID);
        msgParams.Add(count);
        Transform[] t = GetComponentsInChildren<Transform>();
        foreach (Transform child in t)
        {
            child.SendMessage("OnRemoteSelect", msgParams);
        }
    }

    //Handles occupation of remote objects
    void Occupied(NetworkInMessage msg)
    {
        //Read data, write into list, transmit via Message to each child
        long occ = msg.ReadInt64();
        int i = msg.ReadInt32();
        int id = msg.ReadInt32();
        List<object> msgParams = new List<object>();
        msgParams.Add(occ);
        msgParams.Add(i);
        msgParams.Add(id);
        Transform[] t = GetComponentsInChildren<Transform>();
        foreach (Transform child in t)
        {
            child.SendMessage("OnOccupied", msgParams);
        }
    }

    //Handles positional changes of remote objects
    void StageTransform(NetworkInMessage msg)
    {
        // We read the user ID but we don't use it here.
        msg.ReadInt64();

        //Read other data, write into list, transmit via Message to each child
        int id = msg.ReadInt32();
        Vector3 targetPos = CustomMessages.Instance.ReadVector3(msg);
        Quaternion targetRot = CustomMessages.Instance.ReadQuaternion(msg);
        List<object> msgParams = new List<object>();
        msgParams.Add(id);
        msgParams.Add(targetPos);
        msgParams.Add(targetRot);
        Transform[] t = GetComponentsInChildren<Transform>();
        foreach (Transform child in t)
        {
            child.SendMessage("OnStageTransform", msgParams);
        }
    }

    //Handles clients becoming ready to transmit/receive data
    void ClientReady(NetworkInMessage msg)
    {
        //Read data, write into list, transmit via Message to each child
        msg.ReadInt64();
        Transform[] t = GetComponentsInChildren<Transform>();
        foreach (Transform child in t)
        {
            child.SendMessage("OnClientReady");
        }
    }

    void UserCount(NetworkInMessage msg)
    {
        msg.ReadInt64();
        int id = msg.ReadInt32();
        int count = msg.ReadInt32();
        List<object> msgParams = new List<object>();
        msgParams.Add(id);
        msgParams.Add(count);
        Transform[] t = GetComponentsInChildren<Transform>();
        foreach(Transform child in t)
        {
            child.SendMessage("OnUserCount");
        }
    }

    //Handles users finishing their respective tutorials
    void TutorialFinished(NetworkInMessage msg)
    {
        msg.ReadInt64();
        Debug.Log("Received tutorial finished message");
        Transform[] t = GetComponentsInChildren<Transform>();
        foreach (Transform child in t)
        {
            child.SendMessage("OnTutorialFinished");
        }
    }
}
