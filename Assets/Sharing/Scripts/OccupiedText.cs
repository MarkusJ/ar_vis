﻿using UnityEngine;
using System.Collections;

public class OccupiedText : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //Hide initially
        //TODO figure out different way to hide this
	    transform.position = Vector3.Lerp(transform.position, Camera.main.transform.position + Camera.main.transform.up * 20, 0.2f);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void Display()
    {
        //Display text when object is occupied
        transform.position = Vector3.Lerp(transform.position, Camera.main.transform.position + Camera.main.transform.forward*2, 0.2f);
    }
}
