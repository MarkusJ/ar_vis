﻿using HoloToolkit;
using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Keeps track of the current state of the experience.
/// </summary>
public class AppStateManager : Singleton<AppStateManager>
{
    /// <summary>
    /// Enum to track progress through the experience.
    /// </summary>
    public enum AppState
    {
        WaitingForAnchor = 0,
        WaitingForPlaneState,
        Ready
    }

    /// <summary>
    /// Tracks the current state in the experience.
    /// </summary>
    public AppState CurrentAppState { get; set; }

    void Start()
    {
        CurrentAppState = AppState.WaitingForAnchor;
    }

    void Update()
    {
        switch (CurrentAppState)
        {
            case AppState.WaitingForAnchor:
                if (ImportExportAnchorManager.Instance.AnchorEstablished)
                {
                    Debug.Log("Anchor Established. Waiting for observer.");
                    CurrentAppState = AppState.WaitingForPlaneState;
                    //GestureManager.Instance.OverrideFocusedObject = HologramPlacement.Instance.gameObject;
                    //CurrentAppState = AppState.Ready;
                }
                break;
           case AppState.WaitingForPlaneState:
                // Now if we have the stage transform we are ready to go.
                if (SurfaceMeshesToPlanes.Instance.createdPlanes)
                {
                    Debug.Log("Creating planes done. Ready.");
                    CurrentAppState = AppState.Ready;
                    //GestureManager.Instance.OverrideFocusedObject = null;
                    CustomMessages.Instance.SendReady(1);
                }
                break;
        }
    }
}