﻿using UnityEngine;
using System.Collections.Generic;
using HoloToolkit.Sharing;
using System;
using System.Collections;

public class RemoteFoV : MonoBehaviour {

    public class RemoteFoVInstance
    {
        public long UserID;
        public GameObject AssociatedRemoteFoV;
    }

    Dictionary<long, RemoteFoVInstance> fovs = new Dictionary<long, RemoteFoVInstance>();
    void Start()
    {
        //Hide child, Initialize Messagehandlers
        foreach (Renderer r in gameObject.transform.GetComponentsInChildren<Renderer>())
            {
            r.enabled = false;
        }
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.HeadTransform] = this.OnHeadTransformReceived;
        SharingSessionTracker.Instance.SessionLeft += Instance_SessionLeft;
    }

    private void Instance_SessionLeft(object sender, SharingSessionTracker.SessionLeftEventArgs e)
    {
        RemoteFoVInstance cursor;
        //Get cursor for user id
        if (this.fovs.TryGetValue(e.exitingUserId, out cursor))
        {
            Destroy(cursor.AssociatedRemoteFoV);
            this.fovs.Remove(e.exitingUserId);
        }
    }

    private void OnHeadTransformReceived(NetworkInMessage msg)
    {
        //Read User ID from message
        long id = msg.ReadInt64();
        
        //Generate a new remote FoV if needed, or update an existing one
        RemoteFoVInstance fov;
        Transform anchor = ImportExportAnchorManager.Instance.gameObject.transform;
        this.transform.rotation = anchor.rotation;
        Vector3 pos = CustomMessages.Instance.ReadVector3(msg);
        Quaternion rot = CustomMessages.Instance.ReadQuaternion(msg);
        if (!this.fovs.TryGetValue(id, out fov))
        {
            //Create new fov, set model, size and initial position
            fov = new RemoteFoVInstance();
            fov.UserID = id;
            fov.AssociatedRemoteFoV = Instantiate(gameObject.transform.GetChild(0).gameObject); //TODO Add a better model
            fov.AssociatedRemoteFoV.transform.parent = this.transform;
            fov.AssociatedRemoteFoV.transform.position = anchor.TransformPoint(pos);
            fov.AssociatedRemoteFoV.transform.localRotation = rot;
            fovs.Add(id, fov);
            foreach (Collider c in fov.AssociatedRemoteFoV.GetComponentsInChildren<Collider>())
            {
                c.enabled = false;
            }
            foreach (Renderer r in fov.AssociatedRemoteFoV.GetComponentsInChildren<Renderer>())
            {
                r.enabled = false;
            }
        }
        else
        {
            //Update position of FoV box
            if (AppStateManager.Instance.CurrentAppState == AppStateManager.AppState.Ready)
            {
                foreach (Renderer r in fov.AssociatedRemoteFoV.GetComponentsInChildren<Renderer>())
                {
                    r.enabled = true;
                }
            }
            fov.AssociatedRemoteFoV.transform.position = Vector3.Lerp(fov.AssociatedRemoteFoV.transform.position, anchor.TransformPoint(pos), 0.36f);
            fov.AssociatedRemoteFoV.transform.localRotation = rot;
        }
    }


    void Update () {
        if (ImportExportAnchorManager.Instance.AnchorEstablished)
        {
            //Calculate local positions and rotations, transfer them
            Transform anchor = ImportExportAnchorManager.Instance.gameObject.transform;
            Transform headTransform = Camera.main.transform;
            Vector3 headPosition = anchor.InverseTransformPoint(headTransform.position + (headTransform.forward * 2));
            Quaternion headRotation = Quaternion.Inverse(this.transform.rotation) * headTransform.rotation;
            
            CustomMessages.Instance.SendHeadTransform(headPosition, headRotation, 0x1);
        }
	}


}
