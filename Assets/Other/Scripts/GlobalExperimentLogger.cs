﻿using UnityEngine;
using HoloToolkit.Unity;
using HoloToolkit.Sharing;
using System.Collections.Generic;
using System.IO;
using System;
using System.Xml.Serialization;
using UnityEngine.VR.WSA;
using UnityEngine.VR.WSA.Sharing;
//using System.Runtime.Serialization.Formatters.Binary;


#if WINDOWS_UWP
using Windows.Storage;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;

#endif



public class GlobalExperimentLogger : Singleton<GlobalExperimentLogger>
{

    //This will also be the name of the output file
    [Tooltip("Name of the experiment")]
    public string ExperimentName;

    //Elapsed time since experiment has been started
    public float elapsedTime { get; private set; }
    public float cursorWriteIntervalTmp { get; private set; }
    public bool running { get; private set; }
    public string time { get; private set; }
    private string cursorPositionsTmp;

    public string path { get; private set; }
    public string fullPath { get; private set; }
    public long localUserID { get; private set; }
    [Tooltip("Unique username for filename and logging")]
    public string uniqueUserID = "Default";
    public bool loggerReady { get; private set; }
    public int matrixSize { get; private set; }
    public Dictionary<long, List<object>> whoFoundWhen;
    public string anchorName { get; private set; }
    public byte[] anchorData { get; private set; }
    public int anchorLength { get; private set; }
    public bool meshesWritten { get; private set; }

    //All logging events need to be defined here
    public enum LoggingEvents
    {
        Start = 0,
        SelectSingle = 1,
        SelectMulti = 2,
        SelectExclusive = 3,
        SelectPlaceStart = 4,
        SelectPlaceStop = 5,
        ExperimentFinished = 6,
        WrongSelection = 7,

    }

    void Start()
    {
        //Save local user ID, initialize variables
        localUserID = SharingStage.Instance.Manager.GetLocalUser().GetID();
        loggerReady = false;
        whoFoundWhen = new Dictionary<long, List<object>>();
        meshesWritten = false;
        cursorPositionsTmp = "";
    }

    void Awake()
    {
        //TODO Get matrix size and insert it here
        matrixSize = 0;
    }

    void Update()
    {
        //Start timer when client is ready
        if (AppStateManager.Instance.CurrentAppState == AppStateManager.AppState.Ready && !running)
        {
            StartExperimentTimer();
        }
        if (running)
        {
            //Adds time since last tick
            elapsedTime += Time.deltaTime;
            cursorWriteIntervalTmp += Time.deltaTime;
            if (cursorWriteIntervalTmp >= 10.0f)
            {
                cursorWriteIntervalTmp = 0;
                WriteCursorPositionsToFile();
            }

        }
    }


    /// <summary>
    /// Should be called when starting a new experiment
    /// </summary>
    void StartExperimentTimer()
    {
        elapsedTime = 0;
        cursorWriteIntervalTmp = 0;
        long localDate = DateTime.Now.ToFileTime();
        time = localDate.ToString();

        running = true;

        //This will create all needed logfiles; 
        //New asynchronous task for writing a file; has to be called from WINDOWS_UWP
#if WINDOWS_UWP
        var task = Task.Factory.StartNew(async () =>
            {                    
                //Create a separate folder for each experiment
                Windows.Storage.StorageFolder storageFolder = await Windows.Storage.ApplicationData.Current.LocalFolder.CreateFolderAsync(ExperimentName + "_" + time);          
                Windows.Storage.StorageFile logFile = await storageFolder.CreateFileAsync(uniqueUserID + "_" + ExperimentName + "_" + time + ".txt", Windows.Storage.CreationCollisionOption.ReplaceExisting);
                Windows.Storage.StorageFile cursor_data = await storageFolder.CreateFileAsync(uniqueUserID + "_" + ExperimentName + "_" + time + "_cursor_data.txt", Windows.Storage.CreationCollisionOption.ReplaceExisting);
                Windows.Storage.StorageFile vertex_data = await storageFolder.CreateFileAsync(uniqueUserID + "_" + ExperimentName + "_" + time + "_vertex_data.txt", Windows.Storage.CreationCollisionOption.ReplaceExisting);
                Windows.Storage.StorageFile triangle_data = await storageFolder.CreateFileAsync(uniqueUserID + "_" + ExperimentName + "_" + time + "_triangle_data.txt", Windows.Storage.CreationCollisionOption.ReplaceExisting);
                //For saving anchors; currently unneeded
                //Windows.Storage.StorageFile anchor = await storageFolder.CreateFileAsync(uniqueUserID + "_" + ExperimentName + "_" + time + "_anchor.dat", Windows.Storage.CreationCollisionOption.ReplaceExisting);
                //await Windows.Storage.FileIO.WriteBytesAsync(anchor, anchorData);
            });
        task.Wait();
#endif

        LogMeshFilter(SpatialMappingManager.Instance.GetMeshFilters());
        loggerReady = true;
        WriteEvent(localUserID, 0, LoggingEvents.Start);
    }

    /// <summary>
    /// Writing method; will write in currently openend log file. Will need a predefined LoggingEvent.
    /// </summary>
    /// <param name="uID">User ID</param>
    /// <param name="hID">Hologram ID</param>
    /// <param name="e">LoggingEvent that was transferred</param>
    public void WriteEvent(long uID, int hID, LoggingEvents e)
    {
        //Content string, will be filled with log data as experiment progresses
        string content;

        //Wait for logger
        if (loggerReady)
        {
            //Each callable event gets its' own case
            switch (e)
            {
                //New experiment
                case LoggingEvents.Start:
                    content = "New Experiment \"" + ExperimentName + "\" started; my name is " + uniqueUserID + " and I have been assigned ID " + uID;
                    break;
                //Selection of object without shared goal
                case LoggingEvents.SelectSingle:
                    content = elapsedTime.ToString() + " seconds since experiment start - " + "User " + uID + " selected hologram " + hID;
                    break;
                //Selection of object with shared goal; will also log which user found which hologram when
                case LoggingEvents.SelectMulti:
                    content = elapsedTime.ToString() + " seconds since experiment start - " + "User " + uID + " selected multitap hologram ";
                    List<object> tmp = new List<object>();
                    tmp.Add(hID);
                    tmp.Add(elapsedTime);
                    if (!whoFoundWhen.ContainsKey(hID))
                    {
                        whoFoundWhen.Add(uID, tmp);
                    }
                    break;
                //User made an object exclusive to him/herself
                case LoggingEvents.SelectExclusive:
                    content = elapsedTime.ToString() + " seconds since experiment start - " + "User " + uID + " selected hologram " + hID + " to make it exclusive";
                    break;
                //User started placing a hologram
                case LoggingEvents.SelectPlaceStart:
                    content = elapsedTime.ToString() + " seconds since experiment start - " + "User " + uID + " selected hologram " + hID + " to start placing it";
                    break;
                //User finished placing a hologram
                case LoggingEvents.SelectPlaceStop:
                    content = elapsedTime.ToString() + " seconds since experiment start - " + "User " + uID + " selected hologram " + hID + " to finish placing it";
                    break;
                //Experiment over; will log the time each suer needed to finish, if the experiment has been successful, the matrix size, and the total time needed to finish the experiment
                case LoggingEvents.ExperimentFinished:
                    string usersFinishedAfter = "User finished-timestamps:" + Environment.NewLine;
                    foreach (KeyValuePair<long, List<object>> i in whoFoundWhen)
                    {
                        usersFinishedAfter += "User " + i.Key.ToString() + " found target object " + i.Value[0].ToString() + " after " + i.Value[1].ToString() + " seconds" + Environment.NewLine;
                    }
                    content = elapsedTime.ToString() + " seconds since experiment start - " + "User " + uID + " reports the experiment as being successful." + Environment.NewLine
                        + "Matrix size: " + matrixSize.ToString() + Environment.NewLine
                        + "Total time needed for all participants to meet goal: " + elapsedTime.ToString() + Environment.NewLine
                        + usersFinishedAfter;
                    break;
                //User selected a wrong button
                case LoggingEvents.WrongSelection:
                    content = elapsedTime.ToString() + " seconds since experiment start - " + "User " + uID + " selected the wrong button";
                    break;
                //Panic case
                default:
                    content = elapsedTime.ToString() + " seconds since experiment start - " + "Empty log entry";
                    break;
            }

            //New asynchronous task for writing to log file
#if WINDOWS_UWP

        var task = Task.Factory.StartNew(async () =>
            {                    
                Windows.Storage.StorageFolder storageFolder = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFolderAsync(ExperimentName + "_" + time);          
                Windows.Storage.StorageFile logFile = await storageFolder.GetFileAsync(uniqueUserID + "_" + ExperimentName + "_" + time + ".txt");
                await Windows.Storage.FileIO.AppendTextAsync(logFile, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " - " + content + Environment.NewLine);
            });
        task.Wait();

#endif
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="data">Contains anchor data</param>
    /// <param name="length">Anchor data bytearray length</param>
    public void SaveAnchor(byte[] data, int length)
    {
        anchorData = data;
        anchorLength = length;
    }


    //Stub
    public void WriteObjectPosition(Vector3 pos)
    {
        if (loggerReady)
        {
            
        }

    }

    /// <summary>
    /// Will append the received position to a string
    /// </summary>
    /// <param name="pos">Received position</param>
    public void WriteCursorPosition(Vector3 pos)
    {
        //Wait for logger
        if (loggerReady)
        {
            //Extract cursor positional data
            string x = pos.x.ToString();
            string y = pos.y.ToString();
            string z = pos.z.ToString();

            //Write positions to temporary string
            cursorPositionsTmp += x + ";" + y + ";" + z + Environment.NewLine;
        }
    }

    /// <summary>
    /// Will write the current cursorposition-string to a file and reset the string afterwards
    /// </summary>
    public void WriteCursorPositionsToFile()
    {
#if WINDOWS_UWP
        var task = Task.Factory.StartNew(async () =>
            {                   
                Windows.Storage.StorageFolder storageFolder = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFolderAsync(ExperimentName + "_" + time);          
                Windows.Storage.StorageFile logFile = await storageFolder.GetFileAsync(uniqueUserID + "_" + ExperimentName + "_" + time + "_cursor_data.txt");
                await Windows.Storage.FileIO.AppendTextAsync(logFile, cursorPositionsTmp);
                cursorPositionsTmp = "";
            });
        task.Wait();
#endif
    }

    /// <summary>
    /// This will log each available room mesh to a file
    /// </summary>
    /// <param name="meshFilters">List of MeshFilters to work on</param>
    public void LogMeshFilter(List<MeshFilter> meshFilters)
    {
        //Initialize variables
        string vertexFileOutput = "";
        string triangleFileOutput = "";
        List<List<Vector3>> allMeshVertices = new List<List<Vector3>>();
        List<List<int>> allMeshTriangles = new List<List<int>>();

        //Start a new logfile block for each meshfilter
        foreach (MeshFilter i in meshFilters)
        {
            
            List<Vector3> currentMeshVertices = new List<Vector3>();
            foreach (Vector3 v in i.mesh.vertices)
            {
                currentMeshVertices.Add(i.gameObject.transform.TransformPoint(v));
            }
            List<int> currentMeshTriangles = new List<int>();
            foreach (int t in i.mesh.triangles)
            {
                currentMeshTriangles.Add(t);
            }

            //Put everything in lists that are readable from separate threads
            allMeshVertices.Add(currentMeshVertices);
            allMeshTriangles.Add(currentMeshTriangles);
        }

        //Write full string to file
#if WINDOWS_UWP
            //Parent task needed so we can wait for child tasks without freezing
            var parent = Task.Factory.StartNew(async () =>
            {
                foreach(List<Vector3> v in allMeshVertices)
                {
                    int meshIndex = 0;
                        //Lines to separate blocks
                        vertexFileOutput += "New mesh" + Environment.NewLine;

                        //Log every vertice of the current mesh as three positions per line, separated by ";"
                        foreach (Vector3 j in v)
                        {
                            vertexFileOutput += meshIndex + ":" + j.x + ";" + j.y + ";" + j.z + Environment.NewLine;
                            meshIndex++;
                        }
                }   
                foreach(List<int> t in allMeshTriangles)
                {
                        //Lines to separate blocks
                        triangleFileOutput += "New mesh" + Environment.NewLine;

                        //Log triangles of current mesh as indexes separated by ","
                        foreach (int j in t)
                        {
                            triangleFileOutput += j + ",";
                        }
                        triangleFileOutput += Environment.NewLine;
                }   
                //Write to file
                Windows.Storage.StorageFolder storageFolder = await Windows.Storage.ApplicationData.Current.LocalFolder.GetFolderAsync(ExperimentName + "_" + time);          
                Windows.Storage.StorageFile vertexLogFile = await storageFolder.GetFileAsync(uniqueUserID + "_" + ExperimentName + "_" + time + "_vertex_data.txt");
                await Windows.Storage.FileIO.AppendTextAsync(vertexLogFile, vertexFileOutput);
                Windows.Storage.StorageFile triangleLogFile = await storageFolder.GetFileAsync(uniqueUserID + "_" + ExperimentName + "_" + time + "_triangle_data.txt");
                await Windows.Storage.FileIO.AppendTextAsync(triangleLogFile, triangleFileOutput);
                meshesWritten = true;
            });     
#endif
    }
}



